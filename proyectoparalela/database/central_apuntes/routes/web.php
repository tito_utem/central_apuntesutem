<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*
 *Redirige al login de google, y utiliza la funcion redirectToProvider ubicado en el controler LoginController
 */






Auth::routes();

Route::get('redirect', 'SocialAuthGoogleController@redirect');

/*
 * Callback post login la funcion esta en el controlador logincontroler.php
 */
Route::get('/callback', 'SocialAuthGoogleController@callback');


Route::get('/home', 'HomeController@index')->name('home');

