<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Socialite;
use Auth;
use Exception;

class SocialAuthGoogleController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('google')->scopes(['read:user','public_repo'])->redirect();
    }

    public function callback()
    {
        try{
            $googleUser = Socialite::driver('google')->user();
            $existUser = User::where('email',$googleUser->email)->first();

            if($existUser){
                Auth::loginUsingId($existUser->email);
            }
            else{
                $user = new User;
                $user->name = $googleUser ->name;
                $user->email = $googleUser->email;
                $user->password =md5(rand(1,1000));
                $user->save();
                Auth::loginUsingId($user->email);
            }
            return redirect()->to('/home');
        }
        catch(Exception $e){
            return 'error';
        }
    }

}
