<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    protected $documenttype= 'documents_types';
    protected $primaryKey = 'pk';
    public $timestamps = false;
}
