<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents', function(Blueprint $table)
		{
			$table->bigInteger('pk', true);
			$table->bigInteger('type_fk');
			$table->bigInteger('owner_fk');
			$table->bigInteger('course_fk');
			$table->string('code');
			$table->string('path');
			$table->string('name');
			$table->dateTime('created')->default('now()');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('documents');
	}

}
