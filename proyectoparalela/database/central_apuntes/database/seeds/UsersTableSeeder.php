<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * CON DB STATEMENT DESACTIVAMOS LA VERIFICACION DE LA
         * LLAVE FORANEA
         */
        //DB::statement('SET FOREIGN_KEY_CHECK = 0');
        /**
         * Con truncate eliminamos los registros que hizo el seeder
         */
        //DB::table('users')->truncate();

        //for ($i=0; $i < 10; $i++) {
        User::create([
                'img'=>str_random(10),
                'name'=>str_random(10),
                'email'=>'hector.cifuentesm@utem.cl',
                'role'=>rand(0 , 3),
            ]);

        //}
        $user = User::all();
    }
}
