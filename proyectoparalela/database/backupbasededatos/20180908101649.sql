--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Ubuntu 10.5-1.pgdg18.04+1)
-- Dumped by pg_dump version 10.5 (Ubuntu 10.5-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: course ; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public."course " (
    id_course bigint NOT NULL,
    name "char" NOT NULL,
    description "char"
);


ALTER TABLE public."course " OWNER TO hector;

--
-- Name: course _id_course_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public."course _id_course_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."course _id_course_seq" OWNER TO hector;

--
-- Name: course _id_course_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public."course _id_course_seq" OWNED BY public."course ".id_course;


--
-- Name: document; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.document (
    id_document bigint NOT NULL,
    "id_course " bigint NOT NULL,
    name "char" NOT NULL,
    description "char",
    code "char",
    id_doc bigint NOT NULL
);


ALTER TABLE public.document OWNER TO hector;

--
-- Name: document_id_document_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.document_id_document_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.document_id_document_seq OWNER TO hector;

--
-- Name: document_id_document_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.document_id_document_seq OWNED BY public.document.id_document;


--
-- Name: document_type; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.document_type (
    id_doctype bigint NOT NULL,
    type_doc "char" NOT NULL
);


ALTER TABLE public.document_type OWNER TO hector;

--
-- Name: document_type_id_doctype_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.document_type_id_doctype_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.document_type_id_doctype_seq OWNER TO hector;

--
-- Name: document_type_id_doctype_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.document_type_id_doctype_seq OWNED BY public.document_type.id_doctype;


--
-- Name: manage; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.manage (
    id_manage bigint NOT NULL,
    user_id bigint NOT NULL,
    id_course bigint NOT NULL,
    date date
);


ALTER TABLE public.manage OWNER TO hector;

--
-- Name: manage_id_manage_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.manage_id_manage_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.manage_id_manage_seq OWNER TO hector;

--
-- Name: manage_id_manage_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.manage_id_manage_seq OWNED BY public.manage.id_manage;


--
-- Name: role; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.role (
    role_id bigint NOT NULL,
    role "char" NOT NULL
);


ALTER TABLE public.role OWNER TO hector;

--
-- Name: role_role_id_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.role_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_role_id_seq OWNER TO hector;

--
-- Name: role_role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.role_role_id_seq OWNED BY public.role.role_id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public."user" (
    id_user bigint NOT NULL,
    id_role bigint NOT NULL,
    name "char" NOT NULL,
    email "char",
    phone character varying(9)
);


ALTER TABLE public."user" OWNER TO hector;

--
-- Name: user_id_user_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.user_id_user_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_user_seq OWNER TO hector;

--
-- Name: user_id_user_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.user_id_user_seq OWNED BY public."user".id_user;


--
-- Name: course  id_course; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."course " ALTER COLUMN id_course SET DEFAULT nextval('public."course _id_course_seq"'::regclass);


--
-- Name: document id_document; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.document ALTER COLUMN id_document SET DEFAULT nextval('public.document_id_document_seq'::regclass);


--
-- Name: document_type id_doctype; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.document_type ALTER COLUMN id_doctype SET DEFAULT nextval('public.document_type_id_doctype_seq'::regclass);


--
-- Name: manage id_manage; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.manage ALTER COLUMN id_manage SET DEFAULT nextval('public.manage_id_manage_seq'::regclass);


--
-- Name: role role_id; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.role ALTER COLUMN role_id SET DEFAULT nextval('public.role_role_id_seq'::regclass);


--
-- Name: user id_user; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."user" ALTER COLUMN id_user SET DEFAULT nextval('public.user_id_user_seq'::regclass);


--
-- Data for Name: course ; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public."course " (id_course, name, description) FROM stdin;
\.


--
-- Data for Name: document; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.document (id_document, "id_course ", name, description, code, id_doc) FROM stdin;
\.


--
-- Data for Name: document_type; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.document_type (id_doctype, type_doc) FROM stdin;
\.


--
-- Data for Name: manage; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.manage (id_manage, user_id, id_course, date) FROM stdin;
\.


--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.role (role_id, role) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public."user" (id_user, id_role, name, email, phone) FROM stdin;
\.


--
-- Name: course _id_course_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public."course _id_course_seq"', 1, false);


--
-- Name: document_id_document_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.document_id_document_seq', 1, false);


--
-- Name: document_type_id_doctype_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.document_type_id_doctype_seq', 1, false);


--
-- Name: manage_id_manage_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.manage_id_manage_seq', 1, false);


--
-- Name: role_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.role_role_id_seq', 1, false);


--
-- Name: user_id_user_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.user_id_user_seq', 1, false);


--
-- Name: course  course _pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."course "
    ADD CONSTRAINT "course _pkey" PRIMARY KEY (id_course);


--
-- Name: document document_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT document_pkey PRIMARY KEY (id_document);


--
-- Name: document_type document_type_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.document_type
    ADD CONSTRAINT document_type_pkey PRIMARY KEY (id_doctype);


--
-- Name: manage manage_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.manage
    ADD CONSTRAINT manage_pkey PRIMARY KEY (id_manage);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (role_id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id_user);


--
-- Name: document document_id_course _fkey; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT "document_id_course _fkey" FOREIGN KEY ("id_course ") REFERENCES public."course "(id_course);


--
-- Name: document document_id_course _fkey1; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT "document_id_course _fkey1" FOREIGN KEY ("id_course ") REFERENCES public."course "(id_course);


--
-- Name: manage manage_id_course_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.manage
    ADD CONSTRAINT manage_id_course_fkey FOREIGN KEY (id_course) REFERENCES public."course "(id_course);


--
-- Name: manage manage_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.manage
    ADD CONSTRAINT manage_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id_user);


--
-- Name: user user_id_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_id_role_fkey FOREIGN KEY (id_role) REFERENCES public.role(role_id);


--
-- PostgreSQL database dump complete
--

