--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5 (Ubuntu 10.5-1.pgdg18.04+1)
-- Dumped by pg_dump version 10.5 (Ubuntu 10.5-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Tipo_usuario; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public."Tipo_usuario" (
    id_tipo integer NOT NULL,
    nom_tipo "char" NOT NULL
);


ALTER TABLE public."Tipo_usuario" OWNER TO hector;

--
-- Name: TABLE "Tipo_usuario"; Type: COMMENT; Schema: public; Owner: hector
--

COMMENT ON TABLE public."Tipo_usuario" IS 'Tipos de usuario
';


--
-- Name: Tipo_usuario_id_tipo_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public."Tipo_usuario_id_tipo_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Tipo_usuario_id_tipo_seq" OWNER TO hector;

--
-- Name: Tipo_usuario_id_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public."Tipo_usuario_id_tipo_seq" OWNED BY public."Tipo_usuario".id_tipo;


--
-- Name: accede; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.accede (
    id_acceso bigint NOT NULL,
    id_cursos integer NOT NULL,
    date_time timestamp without time zone NOT NULL,
    id_usuario integer NOT NULL
);


ALTER TABLE public.accede OWNER TO hector;

--
-- Name: TABLE accede; Type: COMMENT; Schema: public; Owner: hector
--

COMMENT ON TABLE public.accede IS 'Registro de acceso con hora y fecha de quien accede a la db ';


--
-- Name: accede_id_acceso_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.accede_id_acceso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accede_id_acceso_seq OWNER TO hector;

--
-- Name: accede_id_acceso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.accede_id_acceso_seq OWNED BY public.accede.id_acceso;


--
-- Name: almacena; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.almacena (
    id_almacena integer NOT NULL,
    id_curso integer NOT NULL,
    id_documento integer NOT NULL,
    dia_hora_almacena timestamp without time zone NOT NULL
);


ALTER TABLE public.almacena OWNER TO hector;

--
-- Name: TABLE almacena; Type: COMMENT; Schema: public; Owner: hector
--

COMMENT ON TABLE public.almacena IS 'registro de almacenamiento de los documentos';


--
-- Name: almacena_id_almacena_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.almacena_id_almacena_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.almacena_id_almacena_seq OWNER TO hector;

--
-- Name: almacena_id_almacena_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.almacena_id_almacena_seq OWNED BY public.almacena.id_almacena;


--
-- Name: curso; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.curso (
    "id_curso " integer NOT NULL,
    nombre_curso "char" NOT NULL,
    descripcion "char"
);


ALTER TABLE public.curso OWNER TO hector;

--
-- Name: TABLE curso; Type: COMMENT; Schema: public; Owner: hector
--

COMMENT ON TABLE public.curso IS 'Curso al que pertenece el documento';


--
-- Name: curso_id_curso _seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public."curso_id_curso _seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."curso_id_curso _seq" OWNER TO hector;

--
-- Name: curso_id_curso _seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public."curso_id_curso _seq" OWNED BY public.curso."id_curso ";


--
-- Name: documento; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.documento (
    id_documento integer NOT NULL,
    nombre_documento "char" NOT NULL,
    id_tipo integer
);


ALTER TABLE public.documento OWNER TO hector;

--
-- Name: TABLE documento; Type: COMMENT; Schema: public; Owner: hector
--

COMMENT ON TABLE public.documento IS 'documento almacenado en ladb';


--
-- Name: documento_id_documento_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.documento_id_documento_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documento_id_documento_seq OWNER TO hector;

--
-- Name: documento_id_documento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.documento_id_documento_seq OWNED BY public.documento.id_documento;


--
-- Name: tipo_documento; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.tipo_documento (
    id_tipo integer NOT NULL,
    nom_tipo "char" NOT NULL
);


ALTER TABLE public.tipo_documento OWNER TO hector;

--
-- Name: TABLE tipo_documento; Type: COMMENT; Schema: public; Owner: hector
--

COMMENT ON TABLE public.tipo_documento IS 'Tipo de documento, pdf , txt , docs, etc.';


--
-- Name: tipo_documento_id_tipo_seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public.tipo_documento_id_tipo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_documento_id_tipo_seq OWNER TO hector;

--
-- Name: tipo_documento_id_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public.tipo_documento_id_tipo_seq OWNED BY public.tipo_documento.id_tipo;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: hector
--

CREATE TABLE public.usuario (
    nombre "char" NOT NULL,
    email "char" NOT NULL,
    tipo_usuario integer NOT NULL,
    "id_usuario " integer NOT NULL
);


ALTER TABLE public.usuario OWNER TO hector;

--
-- Name: TABLE usuario; Type: COMMENT; Schema: public; Owner: hector
--

COMMENT ON TABLE public.usuario IS 'Tabla Usuarios';


--
-- Name: usuario_id_usuario _seq; Type: SEQUENCE; Schema: public; Owner: hector
--

CREATE SEQUENCE public."usuario_id_usuario _seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."usuario_id_usuario _seq" OWNER TO hector;

--
-- Name: usuario_id_usuario _seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hector
--

ALTER SEQUENCE public."usuario_id_usuario _seq" OWNED BY public.usuario."id_usuario ";


--
-- Name: Tipo_usuario id_tipo; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."Tipo_usuario" ALTER COLUMN id_tipo SET DEFAULT nextval('public."Tipo_usuario_id_tipo_seq"'::regclass);


--
-- Name: accede id_acceso; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.accede ALTER COLUMN id_acceso SET DEFAULT nextval('public.accede_id_acceso_seq'::regclass);


--
-- Name: almacena id_almacena; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.almacena ALTER COLUMN id_almacena SET DEFAULT nextval('public.almacena_id_almacena_seq'::regclass);


--
-- Name: curso id_curso ; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.curso ALTER COLUMN "id_curso " SET DEFAULT nextval('public."curso_id_curso _seq"'::regclass);


--
-- Name: documento id_documento; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.documento ALTER COLUMN id_documento SET DEFAULT nextval('public.documento_id_documento_seq'::regclass);


--
-- Name: tipo_documento id_tipo; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.tipo_documento ALTER COLUMN id_tipo SET DEFAULT nextval('public.tipo_documento_id_tipo_seq'::regclass);


--
-- Name: usuario id_usuario ; Type: DEFAULT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.usuario ALTER COLUMN "id_usuario " SET DEFAULT nextval('public."usuario_id_usuario _seq"'::regclass);


--
-- Data for Name: Tipo_usuario; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public."Tipo_usuario" (id_tipo, nom_tipo) FROM stdin;
\.


--
-- Data for Name: accede; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.accede (id_acceso, id_cursos, date_time, id_usuario) FROM stdin;
\.


--
-- Data for Name: almacena; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.almacena (id_almacena, id_curso, id_documento, dia_hora_almacena) FROM stdin;
\.


--
-- Data for Name: curso; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.curso ("id_curso ", nombre_curso, descripcion) FROM stdin;
\.


--
-- Data for Name: documento; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.documento (id_documento, nombre_documento, id_tipo) FROM stdin;
\.


--
-- Data for Name: tipo_documento; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.tipo_documento (id_tipo, nom_tipo) FROM stdin;
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: hector
--

COPY public.usuario (nombre, email, tipo_usuario, "id_usuario ") FROM stdin;
\.


--
-- Name: Tipo_usuario_id_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public."Tipo_usuario_id_tipo_seq"', 1, false);


--
-- Name: accede_id_acceso_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.accede_id_acceso_seq', 1, false);


--
-- Name: almacena_id_almacena_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.almacena_id_almacena_seq', 1, false);


--
-- Name: curso_id_curso _seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public."curso_id_curso _seq"', 1, false);


--
-- Name: documento_id_documento_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.documento_id_documento_seq', 1, false);


--
-- Name: tipo_documento_id_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public.tipo_documento_id_tipo_seq', 1, false);


--
-- Name: usuario_id_usuario _seq; Type: SEQUENCE SET; Schema: public; Owner: hector
--

SELECT pg_catalog.setval('public."usuario_id_usuario _seq"', 1, false);


--
-- Name: Tipo_usuario Tipo_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public."Tipo_usuario"
    ADD CONSTRAINT "Tipo_usuario_pkey" PRIMARY KEY (id_tipo);


--
-- Name: accede accede_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.accede
    ADD CONSTRAINT accede_pkey PRIMARY KEY (id_acceso);


--
-- Name: almacena almacena_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.almacena
    ADD CONSTRAINT almacena_pkey PRIMARY KEY (id_almacena);


--
-- Name: documento documento_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.documento
    ADD CONSTRAINT documento_pkey PRIMARY KEY (id_documento);


--
-- Name: tipo_documento tipo_documento_pkey; Type: CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.tipo_documento
    ADD CONSTRAINT tipo_documento_pkey PRIMARY KEY (id_tipo);


--
-- Name: fki_tipo_documento; Type: INDEX; Schema: public; Owner: hector
--

CREATE INDEX fki_tipo_documento ON public.documento USING btree (id_tipo);


--
-- Name: fki_tipo_usuario; Type: INDEX; Schema: public; Owner: hector
--

CREATE INDEX fki_tipo_usuario ON public.usuario USING btree (tipo_usuario);


--
-- Name: documento tipo_documento; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.documento
    ADD CONSTRAINT tipo_documento FOREIGN KEY (id_tipo) REFERENCES public.tipo_documento(id_tipo);


--
-- Name: usuario tipo_usuario; Type: FK CONSTRAINT; Schema: public; Owner: hector
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT tipo_usuario FOREIGN KEY (tipo_usuario) REFERENCES public."Tipo_usuario"(id_tipo);


--
-- PostgreSQL database dump complete
--

