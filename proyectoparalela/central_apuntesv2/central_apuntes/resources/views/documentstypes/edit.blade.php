<!-- edit.blade.php -->
<!--Solo puedes editar el nombre del documento-->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Edition Mode </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Editar un Document</h2><br  />
        <form method="post" action="{{action('DocumentTypeController@update', $pk)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" value="{{$documentstypes->name}}">
          </div>
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Description:</label>
            <input type="text" class="form-control" name="name" value="{{$documentstypes->description}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>
