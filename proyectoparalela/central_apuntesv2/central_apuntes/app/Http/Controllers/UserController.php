<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Undocumented function
     *
     * @return void
     * Muestra una lista de todos los usuarios de la aplicacion
     */

    public function index()
    {
         $users = \App\User::all()->SortBy('pk');

        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasfile('img'))
        {
            $file= $request->file('img');
            $name = $file->getClientOriginalName();
            $file->move(public_path().'/images/',$name);

        }
        //hacer el require pls
        $user= new \App\User;
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->role=$request->get('role');
        $user->img=$name;
        $user->save();
        return redirect('users')->with('Informacion Cargada en la Base de Datos', 'Informacion Agregada con Exito');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     * obtengo todos los usuarios dentro de un array
     */
   /*  public function show(User $user)
    {
        return view('users.show',compact('user'));
    } */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($pk)
    {
        $users = \App\User::find($pk);
        return view('users.edit',compact('users','pk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @param int $pk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$pk)
    {
        $user= \App\User::find($pk);
        $user->name=$request->get('name');
        $user->email=$request->get('email');
        $user->role=$request->get('role');
        $user->save();
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($pk)
    {
        $user = \App\User::find($pk);
        $user->delete();
        return redirect('users')->with('success','Information has been  deleted');
    }
}
