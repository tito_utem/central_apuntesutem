<?php

namespace App\Http\Controllers;

use App\DocumentType;
use Illuminate\Http\Request;

class DocumentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documentstypes=\App\DocumentType::all();

        return view('documentstypes.index',compact('documentstypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('documentstypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $documentstypes= new \App\DocumentType;
        $documentstypes->name=$request->get('name');
        $documentstypes->description=$request->get('description');
        $documentstypes->save();
        return redirect('documentstypes')->with('Informacion Cargada Satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function show(DocumentType $documentType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function edit($pk)
    {
        $documentstypes = \App\DocumentType::find($pk);
        return view('documentstypes.edit',compact('documentstypes','pk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$pk)
    {
        $documentstypes= \App\DocumentType::find($pk);
        $documentstypes->name=$request->get('name');
        //$documentstypes->description->get('description');
        $documentstypes->save();
        return redirect('documentstypes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DocumentType  $documentType
     * @return \Illuminate\Http\Response
     */
    public function destroy($pk)
    {
        $documentstypes= \App\DocumentType::find($pk);
        $documentstypes->delete();
        return redirect('documentstypes')->with('Exito','Informacion Eliminada');
    }
}
